package net.cursar.android.cameralab;

import android.hardware.Camera;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import java.util.List;

public class CameraActivity extends AppCompatActivity {

    private static String LOG_TAG="CameraResTestActivity";

    private SurfaceView preview;
    private SurfaceHolder holder;
    private boolean inpreview;
    private Camera camera;
    boolean justResized=false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        requestWindowFeature(Window.FEATURE_NO_TITLE);

        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_camera);

        inpreview=false;
        preview=(SurfaceView)findViewById(R.id.cameraPreview);
        holder=preview.getHolder();
        holder.addCallback(surfaceHolderCallBack);

    }


    @Override
    protected void onPause() {
        if (inpreview) {
            camera.stopPreview();
        }
        camera.release();
        camera = null;

        inpreview=false;
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        camera = Camera.open();
    }

    SurfaceHolder.Callback surfaceHolderCallBack = new SurfaceHolder.Callback() {
        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            //on surface created, link the camera to the surface
            try{
                camera.setPreviewDisplay(holder);

            }catch (Exception e){
                Log.e(LOG_TAG, "Exception on surfaceCreated", e);
            }

        }

        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

            if(!justResized){
                //on surface changed, rearrange the camera preview
                Camera.Parameters parameters = camera.getParameters();

                Camera.Size size = getOptimalPreviewSize(
                        camera.getParameters().getSupportedPreviewSizes(),
                        width, height);

                if (size != null) {

                    double screenRatio = (double) width / height;
                    double previewRatio = (double) size.width / size.height;

                    int targetWidth;
                    int targetHeight;
                    //the screen ratio should match the preview ratio
                    if (screenRatio > previewRatio) {
                        //then, the height must be increased,
                        targetWidth = width;
                        targetHeight = (int) (((double) width) / previewRatio);
                    } else {
                        //the width must be increased
                        targetHeight = height;
                        targetWidth = (int) (((double) height) * previewRatio);
                    }
                    Log.i(LOG_TAG, "Setting preview size to " + targetWidth + "x" + targetHeight +
                            ", ratio=" + ((double) targetWidth / targetHeight));
                    preview.setLayoutParams(new FrameLayout.LayoutParams(targetWidth, targetHeight));

                    justResized=true;

                    parameters.setPreviewSize(size.width, size.height);
                    camera.setParameters(parameters);
                    camera.startPreview();
                    inpreview = true;
                }else{
                    justResized=false;
                }

            }

        }

        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {
            if(camera!=null) {
                camera.stopPreview();
                camera.release();
                camera = null;
            }
        }

        private Camera.Size getOptimalPreviewSize(List<Camera.Size> sizes, int w, int h) {
            final double ASPECT_TOLERANCE = 0.1;

            double targetRatio=(double)w / h;

            Log.i(LOG_TAG,"Optimal size finding for "+w+"x"+h+" ratio="+targetRatio);

            if (sizes == null) return null;

            Camera.Size optimalSize = null;
            Camera.Size subOptimalSize = null;

            double minDiff = Double.MAX_VALUE;
            double subOptimalMinDiff = Double.MAX_VALUE;

            int targetHeight = h;

            for (Camera.Size size : sizes) {
                double ratio = (double) size.width / size.height;
                Log.i(LOG_TAG,"Testing size of "+size.width+"x"+size.height+" ratio="+ratio);
                if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE) {
                    if (Math.abs(size.height - targetHeight) < subOptimalMinDiff) {
                        subOptimalSize=size;
                        subOptimalMinDiff = Math.abs(size.height - targetHeight);
                    }
                    continue;
                }
                if (Math.abs(size.height - targetHeight) < minDiff) {
                    optimalSize = size;
                    minDiff = Math.abs(size.height - targetHeight);
                    Log.i(LOG_TAG,"Found a size candidate "+size.width+"x"+size.height+" ratio="+ratio);
                }
            }

            if (optimalSize == null) {
                Log.i(LOG_TAG,"Choosing suboptimal size of "+subOptimalSize.width+"x"+
                        subOptimalSize.height+", ratio="+((double)subOptimalSize.width/subOptimalSize.height));
                optimalSize=subOptimalSize;
            }
            return optimalSize;
        }

    };

}
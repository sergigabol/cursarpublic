package net.cursar.android.cameralab;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class CameraActivityOrientationLocalization extends AppCompatActivity {

    private static String LOG_TAG = "CameraOrientLocalization";

    SensorManager sensorManager;
    SensorEventListener sensorEventListener;
    Sensor accelerometer;
    Sensor magnetic;

    float heading;
    float pitch;
    float roll;

    TextView headingText;
    TextView pitchText;
    TextView rollText;

    TextView accXText;
    TextView accYText;
    TextView accZText;

    TextView latText;
    TextView longText;
    TextView altText;
    TextView locAccuracy;

    private SurfaceView preview;
    private SurfaceHolder holder;
    private boolean inpreview;
    private Camera camera;
    boolean justResized = false;

    LocationManager locationManager;
    LocationListener locationListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        requestWindowFeature(Window.FEATURE_NO_TITLE);

        super.onCreate(savedInstanceState);

        Log.i(LOG_TAG, "OnCreate");

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_camera);

        inpreview = false;
        preview = (SurfaceView) findViewById(R.id.cameraPreview);
        holder = preview.getHolder();
        holder.addCallback(surfaceHolderCallBack);


        headingText = (TextView) findViewById(R.id.headingText);
        pitchText = (TextView) findViewById(R.id.pitchText);
        rollText = (TextView) findViewById(R.id.rollText);

        accXText = (TextView) findViewById(R.id.accXText);
        accYText = (TextView) findViewById(R.id.accYText);
        accZText = (TextView) findViewById(R.id.accZText);

        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        accelerometer =
                sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        magnetic = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);

        sensorEventListener = new SensorEventListener() {

            float[] gravityArr;
            float[] magneticArr;

            @Override
            public void onSensorChanged(SensorEvent event) {

                switch (event.sensor.getType()) {
                    case Sensor.TYPE_ACCELEROMETER:
                        gravityArr = event.values;
                        accXText.setText("Accx: " + gravityArr[0]);
                        accYText.setText("Accy: " + gravityArr[1]);
                        accZText.setText("Accz: " + gravityArr[2]);
                        break;
                    case Sensor.TYPE_MAGNETIC_FIELD:
                        magneticArr = event.values;
                        break;
                    default:
                        Log.e(LOG_TAG, "Sensor desconegut "
                                + event.sensor.getType());
                        break;
                }

                if (gravityArr != null && magneticArr != null) {

                    float[] r = new float[9];
                    float[] i = new float[9];

                    boolean success = SensorManager.
                            getRotationMatrix(r, i, gravityArr, magneticArr);

                    if (success) {
                        float[] orientation = new float[3];
                        SensorManager.getOrientation(r, orientation);

                        heading = orientation[0];
                        pitch = orientation[1];
                        roll = orientation[2];


                        headingText.setText(
                                String.format(
                                        getString(R.string.headingLabel)
                                        , Math.toDegrees(heading)
                                )
                        );
                        pitchText.setText(
                                String.format(
                                        getString(R.string.pitchLabel)
                                        , Math.toDegrees(pitch)
                                )
                        );
                        rollText.setText(
                                String.format(
                                        getString(R.string.rollLabel)
                                        , Math.toDegrees(roll)
                                )
                        );

                        Log.i(LOG_TAG, "Oreintation: h=" + heading
                                + ", p=" + pitch + ", r=" + roll);

                    } else {
                        Log.w(LOG_TAG, "Still only one measure");
                    }

                }
            }

            @Override
            public void onAccuracyChanged(Sensor sensor,
                                          int accuracy) {


            }
        };

        latText = (TextView) findViewById(R.id.latText);
        longText = (TextView) findViewById(R.id.longText);
        altText = (TextView) findViewById(R.id.altText);
        locAccuracy = (TextView) findViewById(R.id.accuracyText);

        locationManager = (LocationManager)
                getSystemService(Context.LOCATION_SERVICE);
        locationListener = new LocationListener() {

            Location lastGoodLocation;

            @Override
            public void onLocationChanged(Location location) {
                Log.i(LOG_TAG, "Location changed. lat="
                        + location.getLatitude() + ", long="
                        + location.getLongitude() + ", alt=" +
                        location.getAltitude() + " (accuracy=" +
                        location.getAccuracy() + ")");

                if(isBetterLocation(location, lastGoodLocation)) {
                    lastGoodLocation=location;
                    updateLocTexts(location);
                }else{
                    Toast.makeText(
                            CameraActivityOrientationLocalization.this,
                            "Localization not updated", Toast.LENGTH_SHORT)
                            .show();
                }
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {



            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        };


    }

    private static final int TIME_SIGNNEWER_LIMIT=1000*60;
    private static final int ACCURACY_TRESHOLD=50;

    private boolean isBetterLocation(Location location,
                                     Location lastGoodLocation) {

        if(lastGoodLocation==null) return true;

        long timeDelta = location.getTime()-lastGoodLocation.getTime();

        int accuracyDelta = (int)(
                            location.getAccuracy()-
                                    lastGoodLocation.getAccuracy()
                        );

        boolean isSignificantlyNewer=timeDelta>TIME_SIGNNEWER_LIMIT;
        boolean isNewer=timeDelta>0;

        boolean isLessAccurate=accuracyDelta>0;
        boolean isMoreAccurate=accuracyDelta<0;
        boolean isMuchLessAccurate=accuracyDelta>ACCURACY_TRESHOLD;

        boolean isFromSameProvider=
                location.getProvider()==null?
                        lastGoodLocation.getProvider()==null:
                        location.getProvider().equals(
                                lastGoodLocation.getProvider()
                        );

        if(isSignificantlyNewer)return true;
        if(isMuchLessAccurate)return false;
        if(isMoreAccurate) return true;
        if(isNewer && !isLessAccurate)return true;
        if(isNewer && isFromSameProvider)
            return true;

        return false;
    }


    @Override
    protected void onPause() {

        Log.i(LOG_TAG, "OnPause");

        if (inpreview) {
            camera.stopPreview();
        }
        camera.release();
        camera = null;

        inpreview = false;

        sensorManager.unregisterListener(sensorEventListener);

        try {
            locationManager.removeUpdates(locationListener);
        }catch (SecurityException e){
            Log.e(LOG_TAG,"not enogugh permission",e);
        }
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(LOG_TAG, "OnResume");
        camera = Camera.open();

        sensorManager.registerListener(sensorEventListener,
                magnetic, SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(sensorEventListener,
                accelerometer, SensorManager.SENSOR_DELAY_NORMAL);


        try {

            Location lastLocNet=locationManager.getLastKnownLocation
                    (locationManager.NETWORK_PROVIDER);
            Location lastLocGps=locationManager.getLastKnownLocation
                    (locationManager.GPS_PROVIDER);

            if(isBetterLocation(lastLocNet,lastLocGps)){
                updateLocTexts(lastLocNet);
            }else{
                updateLocTexts(lastLocGps);
            }



            locationManager.requestLocationUpdates(
                    locationManager.NETWORK_PROVIDER,
                    2000,
                    0,
                    locationListener
            );

            locationManager.requestLocationUpdates(
                    locationManager.GPS_PROVIDER,
                    2000,
                    0,
                    locationListener
            );
        }catch (SecurityException e){
            Log.e(LOG_TAG,"Not enoguh permissions to get Location",e);
        }
    }

    SurfaceHolder.Callback surfaceHolderCallBack = new SurfaceHolder.Callback() {
        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            //on surface created, link the camera to the surface
            try{
                camera.setPreviewDisplay(holder);

            }catch (Exception e){
                Log.e(LOG_TAG, "Exception on surfaceCreated", e);
            }

        }

        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

            if(!justResized){
                //on surface changed, rearrange the camera preview
                Camera.Parameters parameters = camera.getParameters();

                Camera.Size size = getOptimalPreviewSize(
                        camera.getParameters().getSupportedPreviewSizes(),
                        width, height);

                if (size != null) {

                    double screenRatio = (double) width / height;
                    double previewRatio = (double) size.width / size.height;

                    int targetWidth;
                    int targetHeight;
                    //the screen ratio should match the preview ratio
                    if (screenRatio > previewRatio) {
                        //then, the height must be increased,
                        targetWidth = width;
                        targetHeight = (int) (((double) width) / previewRatio);
                    } else {
                        //the width must be increased
                        targetHeight = height;
                        targetWidth = (int) (((double) height) * previewRatio);
                    }
                    Log.i(LOG_TAG, "Setting preview size to " + targetWidth + "x" + targetHeight +
                            ", ratio=" + ((double) targetWidth / targetHeight));
                    preview.setLayoutParams(new FrameLayout.LayoutParams(targetWidth, targetHeight));

                    justResized=true;

                    parameters.setPreviewSize(size.width, size.height);
                    camera.setParameters(parameters);
                    camera.startPreview();
                    inpreview = true;
                }else{
                    justResized=false;
                }

            }

        }

        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {
            if(camera!=null) {
                camera.stopPreview();
                camera.release();
                camera = null;
            }
        }

        private Camera.Size getOptimalPreviewSize(List<Camera.Size> sizes, int w, int h) {
            final double ASPECT_TOLERANCE = 0.1;

            double targetRatio=(double)w / h;

            Log.i(LOG_TAG,"Optimal size finding for "+w+"x"+h+" ratio="+targetRatio);

            if (sizes == null) return null;

            Camera.Size optimalSize = null;
            Camera.Size subOptimalSize = null;

            double minDiff = Double.MAX_VALUE;
            double subOptimalMinDiff = Double.MAX_VALUE;

            int targetHeight = h;

            for (Camera.Size size : sizes) {
                double ratio = (double) size.width / size.height;
                Log.i(LOG_TAG,"Testing size of "+size.width+"x"+size.height+" ratio="+ratio);
                if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE) {
                    if (Math.abs(size.height - targetHeight) < subOptimalMinDiff) {
                        subOptimalSize=size;
                        subOptimalMinDiff = Math.abs(size.height - targetHeight);
                    }
                    continue;
                }
                if (Math.abs(size.height - targetHeight) < minDiff) {
                    optimalSize = size;
                    minDiff = Math.abs(size.height - targetHeight);
                    Log.i(LOG_TAG,"Found a size candidate "+size.width+"x"+size.height+" ratio="+ratio);
                }
            }

            if (optimalSize == null) {
                Log.i(LOG_TAG,"Choosing suboptimal size of "+subOptimalSize.width+"x"+
                        subOptimalSize.height+", ratio="+((double)subOptimalSize.width/subOptimalSize.height));
                optimalSize=subOptimalSize;
            }
            return optimalSize;
        }

    };

    private void updateLocTexts(Location location){
        if(location==null)return;

        latText.setText(String.format(getString(R.string.latLabel)
                , location.getLatitude()));
        longText.setText(String.format(getString(R.string.longLabel)
                , location.getLongitude()));
        if(location.hasAltitude()) {
            altText.setText(String.format(getString(R.string.altLabel)
                    , location.getAltitude()));
        }else{
            altText.setText(getString(R.string.altitudeNotAvailable));
        }
        locAccuracy.setText(String.format(getString(R.string.accuracyLabel)
                , location.getAccuracy()));

    }

}
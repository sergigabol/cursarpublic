package net.cursar.android.cameralab;

import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

public class TestThreads extends AppCompatActivity implements View.OnClickListener {

    TextView text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_threads);


        //(findViewById(R.id.buttonCuenta)).setOnClickListener(this);
        (findViewById(R.id.buttonTarea)).setOnClickListener(this);
        (findViewById(R.id.buttonTareaParal)).setOnClickListener(this);
        (findViewById(R.id.buttonTareaAsync)).setOnClickListener(this);

        text=(TextView)findViewById(R.id.textThreads);
    }

    public void startTask(View v){

        try {
            Thread.sleep(6000);
        } catch (InterruptedException e) {
            Log.e("THREAD","Thread interrupted",e);
        }

        text.setText("Despres de la tasca");

    }

    int result=0;
    public void startTaskAsync(View v){

        AsyncTask<Void,Integer,String> task=new AsyncTask<Void,Integer,String>() {
            @Override
            protected String doInBackground(Void[] params) {
                try {
                    Thread.sleep(1000);
                    publishProgress(20);
                    Thread.sleep(1000);
                    publishProgress(40);
                    Thread.sleep(1000);
                    publishProgress(60);
                    Thread.sleep(1000);
                    publishProgress(80);
                    Thread.sleep(1000);

                } catch (InterruptedException e) {
                    Log.e("THREAD","Thread interrupted",e);
                }

                return "Executat en asynctask";
            }

            @Override
            protected void onPostExecute(String o) {
                text.setText(o);
            }

            @Override
            protected void onProgressUpdate(Integer... values) {

                text.setText("La tarea en async va por el "+values[0]+"%");
            }
        };

        task.execute();

    }



    public void startTaskParal(View v){

        final Handler handler=new Handler();

        new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    Thread.sleep(6000);
                } catch (InterruptedException e) {
                    Log.e("THREAD","Thread interrupted",e);
                }
                handler.post(
                        new Runnable() {
                             @Override
                             public void run() {
                                 text.setText(
                                         "Despres de la tasca paralel dins"
                                 );
                             }
                        }
                    );

            }
        }).start();

        text.setText("Despres de la tasca thread");

    }

    int counter=0;

    public void updateCounter(View v){
        counter++;
        ((Button)v).setText("Clickat "+counter);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            /*case R.id.buttonCuenta:
                updateCounter(v);
                break;
            */case R.id.buttonTarea:
                startTask(v);
                break;
            case R.id.buttonTareaParal:
                startTaskParal(v);
                break;
            case R.id.buttonTareaAsync:
                startTaskAsync(v);
                break;
        }

    }
}

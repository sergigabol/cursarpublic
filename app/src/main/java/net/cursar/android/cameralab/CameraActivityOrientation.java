package net.cursar.android.cameralab;

import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.List;

public class CameraActivityOrientation extends AppCompatActivity {

    private static String LOG_TAG="CameraActivityOrientation";

    SensorManager sensorManager;
    SensorEventListener sensorEventListener;
    Sensor accelerometer;
    Sensor magnetic;

    float heading;
    float pitch;
    float roll;

    TextView headingText;
    TextView pitchText;
    TextView rollText;

    TextView accXText;
    TextView accYText;
    TextView accZText;

    private SurfaceView preview;
    private SurfaceHolder holder;
    private boolean inpreview;
    private Camera camera;
    boolean justResized=false;




    @Override
    protected void onCreate(Bundle savedInstanceState) {

        requestWindowFeature(Window.FEATURE_NO_TITLE);

        super.onCreate(savedInstanceState);

        Log.i(LOG_TAG, "OnCreate");

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_camera);

        inpreview=false;
        preview=(SurfaceView)findViewById(R.id.cameraPreview);
        holder=preview.getHolder();
        holder.addCallback(surfaceHolderCallBack);


        headingText=(TextView)findViewById(R.id.headingText);
        pitchText=(TextView)findViewById(R.id.pitchText);
        rollText=(TextView)findViewById(R.id.rollText);

        accXText=(TextView)findViewById(R.id.accXText);
        accYText=(TextView)findViewById(R.id.accYText);
        accZText=(TextView)findViewById(R.id.accZText);

        sensorManager=(SensorManager) getSystemService(SENSOR_SERVICE);
        accelerometer=
                sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        magnetic=sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);

        sensorEventListener=new SensorEventListener() {

            float [] gravityArr;
            float [] magneticArr;

            @Override
            public void onSensorChanged(SensorEvent event) {

                switch (event.sensor.getType()){
                    case Sensor.TYPE_ACCELEROMETER:
                        gravityArr=event.values;
                        accXText.setText("Accx: "+gravityArr[0]);
                        break;
                    case Sensor.TYPE_MAGNETIC_FIELD:
                        magneticArr=event.values;
                        break;
                    default:
                        Log.e(LOG_TAG,"Sensor desconegut "
                                +event.sensor.getType());
                        break;
                }

                if(gravityArr!=null && magneticArr!=null){

                    float [] r=new float[9];
                    float [] i=new float[9];

                    boolean success=SensorManager.
                            getRotationMatrix(r,i,gravityArr,magneticArr);

                    if(success){
                        float [] orientation=new float[3];
                        SensorManager.getOrientation(r,orientation);

                        heading=orientation[0];
                        pitch=orientation[1];
                        roll=orientation[2];


                        headingText.setText(
                                String.format(
                                        getString(R.string.headingLabel)
                                        ,Math.toDegrees(heading)
                                )
                            );
                        pitchText.setText(
                                String.format(
                                        getString(R.string.pitchLabel)
                                        ,Math.toDegrees(pitch)
                                )
                        );
                        rollText.setText(
                                String.format(
                                        getString(R.string.rollLabel)
                                        ,Math.toDegrees(roll)
                                )
                        );

                        Log.i(LOG_TAG,"Oreintation: h="+heading
                                +", p="+pitch+", r="+roll);

                    }else{
                        Log.w(LOG_TAG, "Still only one measure");
                    }

                }
            }

            @Override
            public void onAccuracyChanged(Sensor sensor,
                                          int accuracy) {


            }
        };


    }


    @Override
    protected void onPause() {

        Log.i(LOG_TAG, "OnPause");

        if (inpreview) {
            camera.stopPreview();
        }
        camera.release();
        camera = null;

        inpreview=false;

        sensorManager.unregisterListener(sensorEventListener);

        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(LOG_TAG, "OnResume");
        camera = Camera.open();

        sensorManager.registerListener(sensorEventListener,
                magnetic,SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(sensorEventListener,
                accelerometer,SensorManager.SENSOR_DELAY_NORMAL);
    }

    SurfaceHolder.Callback surfaceHolderCallBack = new SurfaceHolder.Callback() {
        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            //on surface created, link the camera to the surface
            try{
                camera.setPreviewDisplay(holder);

            }catch (Exception e){
                Log.e(LOG_TAG, "Exception on surfaceCreated", e);
            }

        }

        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

            if(!justResized){
                //on surface changed, rearrange the camera preview
                Camera.Parameters parameters = camera.getParameters();

                Camera.Size size = getOptimalPreviewSize(
                        camera.getParameters().getSupportedPreviewSizes(),
                        width, height);

                if (size != null) {

                    double screenRatio = (double) width / height;
                    double previewRatio = (double) size.width / size.height;

                    int targetWidth;
                    int targetHeight;
                    //the screen ratio should match the preview ratio
                    if (screenRatio > previewRatio) {
                        //then, the height must be increased,
                        targetWidth = width;
                        targetHeight = (int) (((double) width) / previewRatio);
                    } else {
                        //the width must be increased
                        targetHeight = height;
                        targetWidth = (int) (((double) height) * previewRatio);
                    }
                    Log.i(LOG_TAG, "Setting preview size to " + targetWidth + "x" + targetHeight +
                            ", ratio=" + ((double) targetWidth / targetHeight));
                    preview.setLayoutParams(new FrameLayout.LayoutParams(targetWidth, targetHeight));

                    justResized=true;

                    parameters.setPreviewSize(size.width, size.height);
                    camera.setParameters(parameters);
                    camera.startPreview();
                    inpreview = true;
                }else{
                    justResized=false;
                }

            }

        }

        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {
            if(camera!=null) {
                camera.stopPreview();
                camera.release();
                camera = null;
            }
        }

        private Camera.Size getOptimalPreviewSize(List<Camera.Size> sizes, int w, int h) {
            final double ASPECT_TOLERANCE = 0.1;

            double targetRatio=(double)w / h;

            Log.i(LOG_TAG,"Optimal size finding for "+w+"x"+h+" ratio="+targetRatio);

            if (sizes == null) return null;

            Camera.Size optimalSize = null;
            Camera.Size subOptimalSize = null;

            double minDiff = Double.MAX_VALUE;
            double subOptimalMinDiff = Double.MAX_VALUE;

            int targetHeight = h;

            for (Camera.Size size : sizes) {
                double ratio = (double) size.width / size.height;
                Log.i(LOG_TAG,"Testing size of "+size.width+"x"+size.height+" ratio="+ratio);
                if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE) {
                    if (Math.abs(size.height - targetHeight) < subOptimalMinDiff) {
                        subOptimalSize=size;
                        subOptimalMinDiff = Math.abs(size.height - targetHeight);
                    }
                    continue;
                }
                if (Math.abs(size.height - targetHeight) < minDiff) {
                    optimalSize = size;
                    minDiff = Math.abs(size.height - targetHeight);
                    Log.i(LOG_TAG,"Found a size candidate "+size.width+"x"+size.height+" ratio="+ratio);
                }
            }

            if (optimalSize == null) {
                Log.i(LOG_TAG,"Choosing suboptimal size of "+subOptimalSize.width+"x"+
                        subOptimalSize.height+", ratio="+((double)subOptimalSize.width/subOptimalSize.height));
                optimalSize=subOptimalSize;
            }
            return optimalSize;
        }

    };

}
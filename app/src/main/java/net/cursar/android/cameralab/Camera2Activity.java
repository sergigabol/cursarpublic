package net.cursar.android.cameralab;

import android.annotation.TargetApi;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Size;
import android.view.Surface;
import android.view.TextureView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Camera2Activity extends AppCompatActivity {

    TextureView preview;
    TextureView.SurfaceTextureListener surfaceTextureListener;
    CameraDevice cameraDevice;
    Size previewSize;
    CameraDevice.StateCallback cameraStateCallback;
    CaptureRequest.Builder captureRequestBuilder;
    CameraCaptureSession.StateCallback previewStateCallback;

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera2);

        previewStateCallback=new CameraCaptureSession.StateCallback() {
            @Override
            public void onConfigured(CameraCaptureSession session) {
                captureRequestBuilder.set(CaptureRequest.CONTROL_MODE,
                        CameraMetadata.CONTROL_MODE_AUTO);

                try {

                    HandlerThread thread=new HandlerThread("CameraPreview");
                    thread.start();
                    Handler background=new Handler(thread.getLooper());

                    session.setRepeatingRequest(captureRequestBuilder.build(),
                            null,
                            background
                            );
                } catch (CameraAccessException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onConfigureFailed(CameraCaptureSession session) {

            }
        };

        cameraStateCallback=new CameraDevice.StateCallback() {
            @Override
            public void onOpened(CameraDevice camera) {
                cameraDevice=camera;

                try {
                    captureRequestBuilder=cameraDevice.
                            createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);

                    SurfaceTexture surfaceTexture=preview.getSurfaceTexture();
                    surfaceTexture.setDefaultBufferSize(
                            previewSize.getWidth(),
                            previewSize.getHeight());
                    Surface surface=new Surface(surfaceTexture);

                    captureRequestBuilder.addTarget(surface);

                    List<Surface> theList=Arrays.asList(surface);
                    cameraDevice.createCaptureSession(
                            theList,
                            previewStateCallback,
                            null
                        );

                } catch (CameraAccessException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onDisconnected(CameraDevice camera) {

            }

            @Override
            public void onError(CameraDevice camera, int error) {

            }
        };

        preview=(TextureView)findViewById(R.id.cameraPreview);
        surfaceTextureListener=new TextureView.SurfaceTextureListener() {

            @Override
            public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
                CameraManager manager =
                        (CameraManager) getSystemService(CAMERA_SERVICE);
                if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.LOLLIPOP) {
                    try {
                        String cameraId = manager.getCameraIdList()[0];
                        CameraCharacteristics characteristics=
                                manager.getCameraCharacteristics(cameraId);
                        StreamConfigurationMap map=
                                characteristics.get(
                                        CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);

                        previewSize=map.getOutputSizes(SurfaceTexture.class)[0];

                        manager.openCamera(cameraId,cameraStateCallback,null);

                    }catch (SecurityException e){
                        Log.e("Camera2","Not enough permissions",e);
                    }catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {

            }

            @Override
            public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
                return false;
            }

            @Override
            public void onSurfaceTextureUpdated(SurfaceTexture surface) {

            }
        };

        preview.setSurfaceTextureListener(surfaceTextureListener);



    }

    @Override
    protected void onPause() {

        if(cameraDevice!=null){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                cameraDevice.close();
            }
            cameraDevice=null;
        }

        super.onPause();
    }
}

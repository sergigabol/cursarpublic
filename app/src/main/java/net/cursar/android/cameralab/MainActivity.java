package net.cursar.android.cameralab;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity
        implements View.OnClickListener{



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.lab1Button).setOnClickListener(this);
        findViewById(R.id.lab2Button).setOnClickListener(this);
        findViewById(R.id.lab3Button).setOnClickListener(this);
        findViewById(R.id.lab4Button).setOnClickListener(this);
        findViewById(R.id.lab5Button).setOnClickListener(this);
        findViewById(R.id.lab6Button).setOnClickListener(this);
        findViewById(R.id.lab7Button).setOnClickListener(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onClick(View v) {
        Intent i;
        switch (v.getId()){
            case R.id.lab1Button:
                i =new Intent(this,CameraActivity.class);
                startActivity(i);
                break;
            case R.id.lab2Button:
                i=new Intent(this,Camera2Activity.class);
                startActivity(i);
                break;
            case R.id.lab3Button:
                i=new Intent(this,CameraActivityOrientation.class);
                startActivity(i);
                break;
            case R.id.lab4Button:
                i=new Intent(this,CameraActivityOrientationLocalization.class);
                startActivity(i);
                break;
            case R.id.lab5Button:
                i=new Intent(this,CameraActivityOrientationGAPILocalization.class);
                startActivity(i);
                break;
            case R.id.lab6Button:
                i=new Intent(this,Compass.class);
                startActivity(i);
                break;
            case R.id.lab7Button:
                i=new Intent(this,TestThreads.class);
                startActivity(i);
                break;
            default:
                Toast.makeText(this,"Button clicked not identified", Toast.LENGTH_LONG).show();
        }
    }
}